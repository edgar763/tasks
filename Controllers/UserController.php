<?php

namespace Controllers;

use Core\DBConnection;
use Model\User;

class UserController
{
    /**
     * Constructor.
     *
     * @param  DBConnection  $db
     *
     * @return void
     */
    public function __construct(DBConnection $db)
    {
        $this->user = new User($db);
    }

    /**
     * Login User.
     *
     * @param  string  $username
     * @param  string  $password
     *
     * @return void
     */
    public function login($username, $password)
    {
        $password = md5($password);
        $id = $this->user->getUserId($username, $password);
        if ($id) {
            $_SESSION['login'] = true;
            $_SESSION['id'] = $id;
        } else {
            $_SESSION['auth_error'] = 'Неправильные учетные данные';
        }
        header('location: /tasks');
    }

    /**
     * Logout user.
     *
     * @return void
     */
    public function logout()
    {
        $_SESSION['login'] = FALSE;
        session_destroy();
        header('location: /tasks');
    }

}
