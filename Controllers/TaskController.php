<?php

namespace Controllers;

use Model\Task;
use Core\View;
use Core\Validator;
use Core\AuthMiddleware;
use Core\DBConnection;

class TaskController
{
    /**
     * Initialize Task Instance.
     * @var $task
     */
    private $task;

    /**
     * TaskController's Constructor.
     *
     * @param  DBConnection  $db
     *
     * @return void
     */
    public function __construct(DBConnection $db)
    {
        $this->task = new Task($db);
    }

    /**
     * Show tasks.
     *
     * @param  string  $route
     * @param  int $page
     * @param  string $order
     *
     * @return void
     */
	public function showResult($route, $page, $order)
    {
        if ($page !== 1)
            $validator['Страница'] = [ "methods" => ["number"] ,  "value" => $page];
        if ($order !== 'id DESC')
            $validator['Сортировка '] = [ "methods" => ["contains"] , "value" => $order , "allowed_values" => ["username DESC", "username ASC", "email ASC", "email DESC","completed ASC","completed DESC"]];
        if (!empty($validator))
            $validatorMessages = Validator::validateInputs($validator);
        if (!empty($validatorMessages)) {
            $_SESSION['error_messages'] = $validatorMessages;
            header('location: /tasks');
        }

        $tasks = $this->task->getTasks($page, $order);
        $totalRows = $this->task->getRowCount();
        $pages = ceil($totalRows/$this->task::LIMIT);
        $result = [
           'tasks'      => $tasks,
           'pages'      => $pages,
           'current'    => $page,
           'next'       => ($pages / $page == 1) ? false : $page + 1,
           'previous'   => ($page == 1) ? false : $page - 1,
           'order'      => $order
        ];

		echo new View("Views/".$route.".php", array("result" => $result));
	}

    /**
     * Edit task.
     *
     * @param  string  $route
     * @param  int  $id
     *
     * @return void
     */

    public function editTask($route, $id)
    {
        if (!AuthMiddleware::checkAuth()) {
            header('location: /tasks');
        } else {
            $task = $this->task->getTaskById($id);
            if ($task)
                echo new View("Views/" . $route . ".php", array("data" => $task));
            else
                echo new View("Views/404.html");
        }
    }

    /**
     * Update task.
     *
     * @param  int  $id
     * @param string $text
     *
     * @return mixed
     */
    public function updateTask($id, $text)
    {
        if (!AuthMiddleware::checkAuth()) {
            header('location: /tasks');
        } else {
            $validator['Текст задачи'] = [ "methods" => ["required"] ,  "value" => $text];
            $validatorMessages = Validator::validateInputs($validator);
            if (!empty($validatorMessages)) {
                $_SESSION['error_messages'] = $validatorMessages;
                header('location: /tasks/' . $id . '/editTask');
            } else {
                $this->task->updateTask($id, $text);
                $_SESSION['success'] = "Задача успешно отредактировано.";
                header('location: /tasks');
            }
        }
    }

    /**
     * Complete task status.
     *
     * @param  int  $id
     *
     * @return mixed
     */
    public function makeTaskCompleted($id)
    {
        if (!AuthMiddleware::checkAuth()) {
            header('location: /tasks');
        } else {
            $task = $this->task->makeTaskCompleted($id);
            return $task;
        }
    }


    /**
     * create task.
     *
     * @param  string  $route
     *
     * @return void
     */
    public function createTask($route)
    {
        echo new View("Views/".$route.".php");
    }

    /**
     * insert task.
     *
     * @param array $request
     *
     * @return mixed
     */
    public function insertTask($request)
    {
        extract($request);
        $validator['Имя пользователя'] = [ "methods" => ["required"] ,  "value" => $username];
        $validator['Email']            = [ "methods" => ["required","validateEmail"] , "value" => $email];
        $validator['Текст задачи']     = [ "methods" => ["required"] ,  "value" => $text];
        $validatorMessages = Validator::validateInputs($validator);
        if (!empty($validatorMessages)) {
            $_SESSION['error_messages'] = $validatorMessages;
            $_SESSION['text']     = $text;
            $_SESSION['username'] = $username;
            $_SESSION['email']    = $email;
            header('location: /tasks/createTask');
        } else {
            $this->task->createTask($username, $email, $text);
            $_SESSION['success'] = "Задача успешно добавлена.";
            header('location: /tasks');
        }
    }
}
