function completeStatus(id) {
    if ($('#status_' + id).is(':checked')) {
        let completed = 1;
        $.ajax({
            type: "POST",
            url: 'tasks/' + id + '/makeTaskCompleted',
            data: {'completed': completed},
            success: function () {
                $('#status_' + id).parent().replaceWith('Да <span class="badge badge-pill badge-info">Отредактировано администратором</span>');
            }
        });
    }
}