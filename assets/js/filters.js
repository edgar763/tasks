function updateQueryStringParameter(key, value) {
    event.preventDefault();
    var uri = document.URL;
    var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
    var separator = uri.indexOf('?') !== -1 ? "&" : "?";
    if (uri.match(re))
        window.location.href = uri.replace(re, '$1' + key + "=" + value + '$2');
    else
        window.location.href = uri + separator + key + "=" + value;
}