<?php include_once 'Views/common/header.php' ?>
<?php include_once 'Views/common/navbar.php' ?>
<div class="container-fluid">
    <?php if (!empty($_SESSION['success'])) {
        ?>
        <div class="row">
            <span class='alert alert-success mt-1 mx-auto'><?= $_SESSION['success'] ?></span>
        </div>
        <?php
    } unset($_SESSION['success']);
    ?>
    <?php if (!empty($_SESSION['error_messages'])) {
        echo "<div class='alert alert-danger'><ul>";
        foreach ($_SESSION['error_messages'] as $message) {
            echo "<li>".$message."</li>";
        }
        echo "</ul></div>";
    }
    unset($_SESSION['error_messages']);
    ?>
    <div class="row pt-1">
        <div class="ml-auto mr-3">
            <a href="tasks/createTask" class="btn btn-success">Создать задачу </a>
        </div>
    </div>
    <div class="form-group">
        <label for="sel1">Сортировать:</label>
        <select class="form-control" onchange="updateQueryStringParameter('order', $(this).val())">
            <option <?php if ($result['order'] == 'username ASC')   echo "selected" ?> value="username ASC">Имя пользователя (по возрастанию)</option>
            <option <?php if ($result['order'] == 'username DESC')  echo "selected" ?> value="username DESC">Имя пользователя (по убыванию)</option>
            <option <?php if ($result['order'] == 'email ASC')      echo "selected" ?> value="email ASC">Email (по возрастанию)</option>
            <option <?php if ($result['order'] == 'email DESC')     echo "selected" ?> value="email DESC">Email (по убыванию)</option>
            <option <?php if ($result['order'] == 'completed ASC')  echo "selected" ?> value="completed ASC">Статус (по возрастанию)</option>
            <option <?php if ($result['order'] == 'completed DESC') echo "selected" ?> value="completed DESC">Статус (по убыванию)</option>
        </select>
    </div>
    <div class="row">
        <div class="col">
            <div class="table-responsive-sm">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Имя пользователя</th>
                        <th>Email</th>
                        <th>Текст задачи</th>
                        <th>Выполнено</th>
                        <?php if (isset($_SESSION['login'])) { ?>
                            <th>Действия</th>
                        <?php }?>
                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($result['tasks'] as $task){ ?>
                            <tr>
                                <td><?=$task['username'];?></td>
                                <td><?=$task['email'];?></td>
                                <td><?=$task['text'];?></td>
                                <td>
                                    <span class="completed-status">
                                        <?php if ($task['completed'] == 1) { ?>
                                            Да <span class="badge badge-pill badge-info">Отредактировано администратором</span>
                                        <?php } else { ?>
                                            Нет
                                            <?php if (isset($_SESSION['login'])) { ?>
                                                <input type="checkbox" id="status_<?= $task['id']?>" onchange="completeStatus(<?= $task['id']?>)" class="status">
                                            <?php } } ?>
                                    </span>
                                </td>
                                <?php if(isset($_SESSION['login'])){ ?>
                                <td><a href="tasks/<?=$task["id"]?>/editTask" class = "btn btn-warning">Редактировать</a>
                                    <?php }?>
                            </tr>
                        <?php }?>
                    </tbody>
                </table>
                <?php if ($result['pages'] > 1): ?>
                    <ul class="pagination float-right">
                        <li class="page-item  <?php if (!$result['previous']) echo "disabled" ?>"><a class="page-link" onclick="updateQueryStringParameter('page',<?= $result['previous']?>)" href="#">Предыдущий</a></li>
                        <?php for($i = 1; $i <= $result['pages']; $i++) { ?>
                            <li class="page-item <?php if($result['current'] == $i) echo "active" ?>"><a class="page-link" onclick="updateQueryStringParameter('page',<?= $i; ?>)" href="#"><?=$i;?></a></li>
                        <?php } ?>
                            <li class="page-item <?php if (!$result['next']) echo "disabled" ?>"><a class="page-link" onclick="updateQueryStringParameter('page',<?= $result['next']?>)" href="#">Следующий</a></li>
                    </ul>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <?php include_once 'Views/common/scripts.php' ?>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
    <script type="text/javascript" src="assets/js/completeStatus.js"></script>
    <script type="text/javascript" src="assets/js/filters.js"></script>
</div>
<?php include_once 'Views/common/footer.php' ?>
