<?php include_once 'Views/common/header.php' ?>
<?php include_once 'Views/common/navbar.php' ?>

<?php if (!empty($_SESSION['error_messages'])) {
    echo "<div class='alert alert-danger'><ul>";
    foreach ($_SESSION['error_messages'] as $message) {
        echo "<li>".$message."</li>";
    }
    echo "</ul></div>";
}
unset($_SESSION['error_messages']);
?>
<div class="container">
    <div class="col-md-8 mx-auto">
        <h1 class="text-center">Создать задачу</h1>
        <form id = "createForm" class="form-horizontal" action="/tasks/insertTask" method="POST">
            <div class="form-group">
                <label class="control-label" for="task_username">Имя пользователя*:</label>
                <div >
                    <input name="username" <?php if (!empty($_SESSION['username'])): ?> value="<?= $_SESSION['username'];?>" <?php unset($_SESSION['username']); endif; ?> type="text" class="form-control"  id="task_username" >
                </div>
            </div>
            <div class="form-group">
                <label class="control-label" for="email">Email*:</label>
                <div>
                    <input name="email" <?php if (!empty($_SESSION['email'])): ?> value="<?= $_SESSION['email'];?>" <?php unset($_SESSION['email']); endif; ?> type="email" class="form-control" id="email" >
                </div>
            </div>
            <div class="form-group">
                <label class="control-label" for="text">Текст задачи*:</label>
                <div>
                    <textarea name="text" class="form-control" id="text" rows="5"><?php if (!empty($_SESSION['text'])) echo $_SESSION['text']; unset($_SESSION['text']); ?></textarea>
                </div>
            </div>
            <div class="form-group error_div">
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-success update">Сохранить</button>
                </div>
            </div>
        </form>
    </div>
</div>
<?php include_once 'Views/common/scripts.php' ?>
<?php include_once 'Views/common/footer.php' ?>