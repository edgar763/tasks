<?php

namespace Core;

class DBConnection
{
	/**
	 * @var $connection;
	 */
	public $connection;

	/**
	 * DBConnector's Constructor.
	 *
	 * @return mixed
	 */
	public function __construct()
	{
       try {
   			$this->connection = new \PDO("mysql:host=localhost;dbname=tasks", "root", "edgar763");
   			$this->connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
   			return $this->connection;
   		} catch(\PDOException $e) {
		   	echo "Connection failed: " . $e->getMessage();
		   }
		}
}

?>
