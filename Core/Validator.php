<?php

namespace Core;

class Validator
{
    /**
     * Create messages array
     */
    private static $responseMessage = [];

    /**
     * Validate Inputs
     *
     * @param array $inputs
     *
     * @return  array
     */
    public static function validateInputs($inputs)
    {
        foreach ($inputs as $key => $input) {
            $trimmedInput = trim($input["value"]);
            foreach ($input["methods"] as $method) {
                ($method == "contains") ? self::$method($trimmedInput , $input["allowed_values"], $key) : self::$method($trimmedInput , $key);
            }
        }
        return self::$responseMessage;
    }

    /**
     * Check if required
     *
     * @param string $value
     * @param string $key
     *
     * @return void
     */
    private static function required($value, $key)
    {
        if( !strlen($value) > 0 )
            self::formatMessage($key,$key . " должно быть заполнено");
    }

    /**
     * Check if required
     *
     * @param string $email
     *
     * @return void
     */
    public static function validateEmail($email, $key)
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL))
            self::formatMessage($key,$key . " не является валидным email-ом ");
    }

    /**
     * Check if number meets specified format
     *
     * @param string $value
     * @param string $key
     *
     *  @return void
     */
    private static function number($value, $key)
    {
        if (!preg_match('/^\d+$/',$value))
            self::formatMessage($key, $key . ' должно являться числом');
    }

    /**
     * Check if given values contain value
     *
     * @param string $value
     * @parm array $allowed_values
     * @param string $key
     *
     *  @return void
     */
    private static function contains($value, $allowed_values, $key)
    {
        if (!in_array($value, $allowed_values))
            self::formatMessage($key,$key . ' вне списка возможных значений');
    }

    /**
     * Format Message
     *
     * @param string $message
     * @param string $key
     *
     *  @return void
     */
    private static function formatMessage($key , $message)
    {
        self::$responseMessage[$key] = $message;
    }

}
