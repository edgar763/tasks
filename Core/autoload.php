<?php
    spl_autoload_register(function($class) {
        $class = str_replace("\\", '/', $class);
        $file =  $_SERVER['DOCUMENT_ROOT'].'/'. $class . '.php';
        if (is_file($file)) {
            require_once $file;
        }
    });