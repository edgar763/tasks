<?php

namespace Core;

use Controllers\TaskController;
use Controllers\UserController;

class Router {

    /**
     * List of Possible routes.
     */
	private static $routes =
	[
	    'tasks',
		'createTask',
		'editTask',
		'updateTask',
		'makeTaskCompleted',
		'insertTask',
		'login',
		'logout'
	];

    /**
     * Get current route.
     *
     * @param string $route
     *
     * @return mixed
     */
	public static function getRoute($route)
    {
		$url = trim($route['path'], '/');
    	$urlSegments = explode('/', $url);
        parse_str($route['query'] ?? '', $queryString);
    	if (in_array($route['path'], self::$routes)) {
			return self::getRouteClass($route['path'], null , $queryString);
		} elseif (isset($urlSegments[2]) && in_array($urlSegments[2], self::$routes)) {
			return self::getRouteClass($urlSegments[2],$urlSegments[1]);
		} elseif (isset($urlSegments[1]) && in_array($urlSegments[0], self::$routes)) {
			return self::getRouteClass($urlSegments[1]);
        } else {
            include("Views/404.html");
        }
	}

    /**
     * Get class from given route.
     *
     * @param string $path
     * @param array $queryString
     * @param int $id
     *
     * @return mixed
     */
	private static function getRouteClass($path, $id=null, $queryString=[])
    {
        switch ($path) {
            case 'tasks':
                return (new TaskController(new DBConnection))->showResult($path, $queryString['page'] ?? 1,$queryString['order'] ?? 'id DESC');
                break;
            case 'createTask':
                return (new TaskController(new DBConnection))->createTask($path);
                break;
            case 'editTask':
                return (new TaskController(new DBConnection))->editTask($path, $id);
                break;
            case 'updateTask':
                return (new TaskController(new DBConnection))->updateTask($id, $_POST['text']);
                break;
            case 'makeTaskCompleted':
                return (new TaskController(new DBConnection))->makeTaskCompleted($id);
                break;
            case 'insertTask':
                return (new TaskController(new DBConnection))->insertTask($_POST);
                break;
            case 'login':
                return (new UserController(new DBConnection))->login($_POST['username'],$_POST['password']);
                break;
            case 'logout':
                return (new UserController(new DBConnection))->logout();
                break;
        }
	}
}
