<?php

namespace Model;

use Core\DBConnection;

class Task
{
    /**
     * Limit result rows.
     */
    const LIMIT=3;

    /**
     * Initialize Db Instance.
     */
	private $connection;

    /**
     * Constructor.
     *
     * @param  DBConnection  $db
     *
     * @return void
     */
    public function __construct(DBConnection $db)
    {
        $this->connection = $db;
    }

    /**
     * Get tasks.
     *
     * @param int $page
     * @param string $order
     *
     * @return array
     */
	public function getTasks($page, $order)
    {
        $offset = ($page-1) * 3;
		$sth = $this->connection->connection->prepare("SELECT * FROM `tasks` ORDER BY $order LIMIT ".self::LIMIT." OFFSET $offset");
		$sth->execute();
        $tasks = $sth->fetchAll(\PDO::FETCH_ASSOC);
	    return $tasks;
	}

    /**
     * get task by id.
     *
     * @param int $id
     *
     * @return array
     */
    public function getTaskById($id)
    {
        $sth = $this->connection->connection->prepare("SELECT * FROM `tasks` WHERE id =".$id);
        $sth->execute();
        return $sth->fetch(\PDO::FETCH_ASSOC);
    }

    /**
     * Update Task.
     *
     * @param int $id
     * @param string $text
     * @return mixed
     */
    public function updateTask($id, $text)
    {
        $sql = "UPDATE tasks SET 
        text = :text
        WHERE id = :id";
        $text = htmlspecialchars($text, ENT_QUOTES, 'UTF-8');
        $stmt = $this->connection->connection->prepare($sql);
        $stmt->bindParam(':text',$text , \PDO::PARAM_STR);
        $stmt->bindParam(':id', $id, \PDO::PARAM_INT);
        return $stmt->execute();
    }

    /**
     * Update Task Status.
     *
     * @param int $id
     * @return boolean
     */
    public function makeTaskCompleted($id)
    {
        $sql = "UPDATE tasks SET 
        completed = 1
        WHERE id = :id";

        $stmt = $this->connection->connection->prepare($sql);
        $stmt->bindParam(':id', $id, \PDO::PARAM_INT);
        $stmt->execute();

        return $stmt->execute();
    }

    /**
     * Create Task.
     *
     * @param string $username
     * @param string $email
     * @param string $text
     *
     * @return mixed
     */
    public function createTask($username, $email, $text)
    {
        $username = htmlspecialchars($username, ENT_QUOTES, 'UTF-8');
        $text   = htmlspecialchars($text, ENT_QUOTES, 'UTF-8');
        $stmt = $this->connection->connection->prepare("INSERT INTO tasks (username,email,text) VALUES (:username, :email, :text)");
        $stmt->bindParam(':username', $username, \PDO::PARAM_STR);
        $stmt->bindParam(':email', $email, \PDO::PARAM_STR);
        $stmt->bindParam(':text', $text, \PDO::PARAM_STR);

        return $stmt->execute();
    }

    /**
     * Get row count.
     *
     * @return int
     */
    public function getRowCount()
    {
        return $this->connection->connection->query("SELECT count(*) FROM `tasks`")->fetchColumn();
    }

}
