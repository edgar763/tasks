<?php

namespace Model;

use Core\DBConnection;

class User
{
    /**
     * Initialize Db Instance.
     */
    private $connection;

    /**
     * Constructor.
     *
     * @param  DBConnection  $db
     *
     * @return void
     */
    public function __construct(DBConnection $db)
    {
        $this->connection = $db;
    }

    /**
     * Get User
     *
     * @param string $username
     * @param string $password
     *
     * @return int
     */
    public function getUserId($username, $password)
    {
        $sth = $this->connection->connection->prepare("SELECT id from admin WHERE username='$username' and password='$password'");
        $sth->execute();
        return $sth->fetchAll()[0]['id'];
    }
}
